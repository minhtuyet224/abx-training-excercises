#https://leetcode.com/problems/sum-of-left-leaves/
#written by: minhtuyet224

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def sumOfLeftLeaves(self, root):
        res=0
        if root==None: 
            return res  
        res=self.sumOfLeftLeaves(root.left)  + self.sumOfLeftLeaves(root.right)
        if root.left and not root.left.right and not root.left.left:
            res+=root.left.val      
        return res

