#https://leetcode.com/problems/move-zeroes/
#written by: minhtuyet224

class Solution:
    def moveZeroes(self, nums):
        """
       sap xep all except key=0,
       vi vay all numbers 0 moves to the end
        """
        index=0
        for i in range(len(nums)):
            if nums[i]!=0:
                nums[index],nums[i]=nums[i],nums[index]
                index+=1

