#https://leetcode.com/problems/add-digits/
#written by: minhtuyet224

class Solution:
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        if num<10:
            return num
        else:
            return self.addDigits(num%10+num//10)
        
